# Типы экономик:
* __Инфиляция__ - количество монет увеличивается
    * минус цена монет падает
    * плюс стимулирует людей тратить
* __Дефиляция__ - количество монет фиксированое или уменьшаеться
    * плюс цена всегда растет
    * минус стимулирует людей к накоплению
## примеры
* bitcoin - всего 21 млн монет, добываются майнерами
*
# Типы консенсуса:

__Proof-of-Work (POW) [bitcoin, litecoin, ethereum]__

    Главная особенность этих схем заключается в асимметрии затрат времени — длительность для инициатора запроса и высокая скорость для ответа.
    Подобные схемы также известны как client puzzle (функция клиентской головоломки), computational puzzle (вычислительная головоломка), или CPU pricing function.
    Ещё раз - суть в разности затрат времени, то есть мы долго что-то вычисляем, но можем потом это некоторым образом быстро проверить.

__Proof-of-Stake (POS) [nxt, peercoin]__

    По сути, процесс PoS-майнинга является лотереей. Однако за билет не нужно «платить» своими мощностями
    На ваши шансы влияют лишь ваше общее число монет и текущая сложность сети.

__Delegated-POS (DPOS) [BitShares, Slasher, Tendermint]__

    В этих протоколах, блоки порождаются предопределенным множеством пользователей системы (делегатами), которые получают вознаграждение за свою обязанность и наказываются за злонамеренное поведение.
    В то время как блок создается отдельным пользователем, чтобы считаться действительным, он обычно должен быть подписан более чем одним делегатом. Список пользователей, подходящих для подписывания блоков, периодически изменяется в соответствии с определенными правилами.

__Proof-of-Activity (POA)__

    В PoA каждый блок является продуктом совместного участия как PoW, так и PoS-майнера

__Proof-of-Burn (POB)__

    «Сжигание» происходит путем отправки денег на такой адрес, с которого гарантированно нельзя их потратить.
    Итак, избавляясь таким образом от своих монет, вы получаете право на пожизненный майнинг, который тоже устроен как лотерея среди всех владельцев сожженных монет. И, понятно дело, чем больше вы сожгли – тем больше ваши шансы.

__Proof-of-Capacity (POC)__

    представляет собой реализацию популярной идеи «мегабайты как ресурсы».

__Proof-of-Storage (POS)__

    выделенное место используется всеми участниками как совместное облачное хранилище.

__Proof-of-Service (POS) [dash]__

    Концепция впервые применена в криптовалюте DASH.
    Здесь, чтобы получать эту часть награды, надо запустить мастерноду и приобрести 1000 монет DASH.
    Монеты должны лежать на счету без движения, как только вы потрарите их или даже небольшую их часть, вы перестанете получать награду как мастернода во Proof-of-Service.
    Основная функция мастерноды - это выполнять анонимизацию транзакций по протоколу Darksend и подписывать транзакции (практически моментально).
    Также, мастерноды участвуют в голосованиях, и в будущем у них будет больше функций.

__Transactions as POS (TaPOS) [Graphene]__

    В TaPoS все транзакции включают в себя хеш последнего блока и считаются недействительными, если этот блок отсутствует в истории цепи. Любой, кто подписывает транзакцию, находясь на покинутом форке, обнаружит, что транзакция недействительна и не может перейти на основной форк.

__Proof-of-Listen (POL) [http://soundchain.org]__

    подразумевает прямые выплаты музыкантам за факт прослушивания их треков пользователями.

__Proof-of-importance (POI)__

    user's importance is determined by how many coins they have and the number of transactions made to and from their wallet
    https://themerkle.com/what-is-proof-of-importance/
    https://www.smithandcrown.com/definition/proof-of-importance/
    https://www.nem.io/NEM_techRef.pdf#section.7

__Delayed Proof of Work (dPoW)__

    https://supernet.org/en/technology/whitepapers/delayed-proof-of-work-dpow#delayed-proof-of-work-etails

__Proof-of-Belief (POB)__

    https://www.ledgerjournal.org/ojs/index.php/ledger/article/view/37

__Proof of Elapsed Time (PoET)__

    который использует модуль доверенных вычислений SGX, встроенный в процессоры Intel последних поколений.


# Заметки

__tinycoin__ на питоне написаная реализация блокчейна для обучения

    https://github.com/JeremyRubin/tinycoin

__The MIT Digital Currency Initiative @ Media Lab__

    https://github.com/mit-dci

__lightning-network__

    тех описание
    https://lightning.network/lightning-network-paper.pdf


__Консенсус__

    метод принятие решения на основе общего согласия.

__Концепция Кроссчейн__

    https://github.com/polkadot-io/polkadotpaper/blob/master/PolkaDotPaper.pdf

__список блокчейн исходников__

    https://www.slideshare.net/BrunoRicardo9/blockchain-code?qid=8218a9c5-6edb-46ef-97c2-5ed32ad66897&v=&b=&from_search=4

__nimiq__

    крипта в браузере
    https://nimiq.com/betanet/

__cuckoo cycle__

    система asic resist для pow опирается на требование к памяти(ram)

__самомайнейщися умный контракт :3__

    https://www.minereum.com

__cosmos__

    https://cosmos.network
    просто космос... взаимодействие сети блокчейнов
    хорошая подробная документация
    https://github.com/cosmos/cosmos

__полезные ссылки__

    https://github.com/BumblebeeBat/FlyingFox куча документации и описаний

    https://github.com/LucasIsasmendi/portfolio/tree/master/blockchain книга по крипте

    http://www.truthcoin.info список критики по смартам и статейки

    куча не сортированых ссылок и текста

    http://diyhpl.us/wiki/bitcoin/big-pile/
    http://diyhpl.us/~bryan/irc/bitcoin/bitcoin-selected-bookmarks.2015-09-09.txt
    https://github.com/DavidVorick/knosys








# whitepaper
__Примеры whitepaper__
* __dentacoin__
https://www.dentacoin.com/white-paper/Whitepaper-ru%20v.0.9.pdf
* __EOS__
https://github.com/EOSIO/Documentation/blob/master/ru-RU/TechnicalWhitePaper.md
* __aeternity__
https://blockchain.aeternity.com/æternity-blockchain-whitepaper.pdf

# Смарт контракты
* __EOS__

    перешли на webassembly c Wren
    50 000 последовательных действий в секунду
    https://golos.io/ru--blokcheijn/@rusteemitblog/web-assembly-na-eos-50-000-perevodov-v-sekundu-perevod-stati-dantheman

    кучка ссылок по EOS
    http://telegra.ph/Podborka-informacii-po-EOS-05-21

* __Ethereum__

Ethereum Virtual Machine (EVM)
Smart contracts are high-level programming abstractions that are compiled down to EVM bytecode and deployed to the Ethereum blockchain for execution. They can be written in Solidity (a language library with similarities to C and JavaScript), Serpent (similar to Python), LLL (a low-level Lisp-like language), and Mutan (Go-based, but deprecated). There is also a research-oriented language under development called Viper (a strongly-typed Python-derived decidable language).

Corda написана на Kotlin и поддерживает смарт-контракты на любом JVM-совместимом языке.

* __vavilon__

в разработке отсоединяет контракты от блокчейна evm
https://vavilon.site/Vavilon_whitepaper_rus.pdf

* __BOScoin__

https://medium.com/boscoin/trust-contracts-human-readable-machine-readable-smart-contracts-4cec25453a30

* __Aeternity__

Используется стековая виртуальная машина (VM), а также скриптовый язык, подобный
Forth и биткоину, хотя в сравнении с последним он несколько богаче. VM поддерживает
функции, а не goto, в результате чего семантика легче поддаётся анализу. Список кодов
операций (OP_CODES) виртуальной машины можно найти на нашей странице в GitHub3.
Кроме того, существует Forth-подобный язык Chalang более высокого уровня, который
компилирует байткод для VM. Он поддерживает макросы и переменные имена, однако
сохраняет при этом стековую модель исполнения

* __Hyperledger Burrow__

is a permissioned Ethereum smart-contract blockchain node built with <3 by Monax. It executes Ethereum smart contract code on a permissioned virtual machine. Burrow provides transaction finality and high transaction throughput on a proof-of-stake Tendermint consensus engine.
https://github.com/hyperledger/burrow

* __атаки на смарты__

https://www.slideshare.net/EnsRationis2020/ethereum-70019593

* __про ико__

http://happycoin.club/prost-kak-pyat-kopeek-za-chto-kritikuyut-standart-erc-20/

* __про bancor__

https://www.bancor.network/static/faq/Bancor_Protocol_FAQ_ru.pdf

* __ERC20_Token_Standard__

https://theethereum.wiki/w/index.php/ERC20_Token_Standard



# Тренды
* __Tendermint__ оч хорошая документация

https://tendermint.com/docs/internals/byzantine-consensus-algorithm
* __Slasher__ первая версия того что хотели внедрить в эфир

https://blog.ethereum.org/2014/10/03/slasher-ghost-developments-proof-stake/
* __Casper__ алгоритм который собираются внедрить в эфир

    * http://rchain-architecture.readthedocs.io/en/latest/execution_model/consensus_protocol.html
    * https://www.deepdotweb.com/2017/01/15/casper-proof-stake-pos-consensus-protocol-implementation-ethereum/
    * https://docs.google.com/document/d/1Xf_iHGI51Lp6nVHPZo0SdUw60-NA6YJUvk2s-eYaxLE/edit
* __tezos__
* __EOS__
* __Aeternity__
* __Steem__ блого плтформа на основе блокчейна
* __NEM__
    * краткое описание работы

    https://golos.io/ru--kriptovalyuty/@ontofractal/til-ob-algoritme-konsensusa-i-vozmozhnostyakh-kriptotokena-nem
    * техническое описание

    https://www.nem.io/NEM_techRef.pdf

# ICO списки
* https://etherscan.io/tokens
* https://icotracker.net/past
* https://www.coinschedule.com
* https://www.ico-list.com
* http://icorating.com
* https://icobazaar.com/list
* https://icostats.com

# источники
* __Краткое описание консесусов__

https://golos.io/ru--kriptovalyuta/@sxiii/kripta-chto-takoe-prostymi-slovami-proof-of-work-proof-of-stake-proof-of-activity-proof-of-burn-proof-of-capacity-proof-of
* __более подробное описание POA__

https://habrahabr.ru/post/266619/
* __более подробное описание POS POW__

https://habrahabr.ru/post/265561/
* __переводы__

https://golos.io/@rusteemitblog

* __более техническое описание POA__

http://eprint.iacr.org/2014/452.pdf

* __полезные ссылки__
    * http://www.bitcoin.ninja - немного ссылок
    * http://www.jbonneau.com/doc/BMCNKF15-IEEESP-bitcoin.pdf - хороший сводный обзор
    * https://docs.google.com/spreadsheets/d/1VaWhbAj7hWNdiE73P-W-wrl5a0WNgzjofmZXe0Rh5sg/edit?pli=1#gid=0 - исследования
    * http://www.jbonneau.com/doc/BMCNKF15-IEEESP-bitcoin.pdf
    SoK: Research Perspectives and Challenges for Bitcoin and Cryptocurrencies
